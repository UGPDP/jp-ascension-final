﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public int peopleWaiting;

    public static int points;

    public Text pointsText;
    //public Animator animComp;

    Spawn spawnCompL;
    Spawn spawnCompR;

    bool difficulty1 = false;
    bool difficulty2 = false;
    bool difficulty3 = false;
    bool difficulty4 = false;


    void Start()
    {
       // animComp.Play("end");
        spawnCompL = GameObject.Find("Spawner1").GetComponent<Spawn>();
        spawnCompR = GameObject.Find("Spawner2").GetComponent<Spawn>();
    }

    void Update()
    {
        if(peopleWaiting >= 10)
        {
            SceneManager.LoadScene("LoseMenu");
        }

        if(points == 500 && difficulty1 == false)
        {
            spawnCompL.spawnRate = spawnCompL.spawnRate * 0.95f;
            spawnCompR.spawnRate = spawnCompL.spawnRate * 0.95f;
            difficulty1 = true;
        }
        if (points == 1500 && difficulty2 == false)
        {
            spawnCompL.spawnRate = spawnCompL.spawnRate * 0.95f;
            spawnCompR.spawnRate = spawnCompL.spawnRate * 0.95f;
            difficulty2 = true;
        }
        if (points == 3000 && difficulty3 == false)
        {
            spawnCompL.spawnRate = spawnCompL.spawnRate * 0.95f;
            spawnCompR.spawnRate = spawnCompL.spawnRate * 0.95f;
            difficulty3 = true;
        }
        if (points == 6000 && difficulty4 == false)
        {
            spawnCompL.spawnRate = spawnCompL.spawnRate * 0.95f;
            spawnCompR.spawnRate = spawnCompL.spawnRate * 0.95f;
            difficulty4 = true;
        }
    }

    public void GetPoints()
    {
        points += 50;
    }

    public void SetPointsText()
    {
        pointsText.text = "Puntos "  + points.ToString();
    }
}
