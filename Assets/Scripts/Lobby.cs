﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lobby : MonoBehaviour
{
    public List<Transform> waitPoints;
    public bool point1taken;
    public bool point2taken;
    public bool point3taken;
    public bool point4taken;
    public bool point5taken;
    public bool point6taken;
    public bool point7taken;
    public bool point8taken;
    public bool point9taken;

    public int currentPeopleOnQueue;
    
    public Transform nextPoint;
    
    void Start()
    {
        
    }
    void Update()
    {
        CheckPeopleInQueue();

        if (currentPeopleOnQueue <= 0)
        {
            nextPoint = waitPoints[0];
            currentPeopleOnQueue = 0;
        }
        if (currentPeopleOnQueue >= 1)
        {
            point1taken = true;
        }
        else
        {
            point1taken = false;
        }
        if (currentPeopleOnQueue >= 2)
        {
            point2taken = true;
        }
        else
        {
            point2taken = false;
        }
        if (currentPeopleOnQueue >= 3)
        {
            point3taken = true;
        }
        else
        {
            point3taken = false;
        }
        if (currentPeopleOnQueue >= 4)
        {
            point4taken = true;
        }
        else
        {
            point4taken = false;
        }
        if (currentPeopleOnQueue >= 5)
        {
            point5taken = true;
        }
        else
        {
            point5taken = false;
        }
        if (currentPeopleOnQueue >= 6)
        {
            point6taken = true;
        }
        else
        {
            point6taken = false;
        }
        if (currentPeopleOnQueue >= 7)
        {
            point7taken = true;
        }
        else
        {
            point7taken = false;
        }
        if (currentPeopleOnQueue >= 8)
        {
            point8taken = true;
        }
        else
        {
            point8taken = false;
        }
        if (currentPeopleOnQueue >= 9)
        {
            point9taken = true;
        }
        else
        {
            point9taken = false;
        }
    }

    void CheckPeopleInQueue()
    {
        if (point1taken == true)
        {
            nextPoint = waitPoints[1];
        }
        if (point2taken == true)
        {
            nextPoint = waitPoints[2];
        }
        if (point3taken == true)
        {
            nextPoint = waitPoints[3];
        }
        if (point4taken == true)
        {
            nextPoint = waitPoints[4];
        }
        if (point5taken == true)
        {
            nextPoint = waitPoints[5];
        }
        if (point6taken == true)
        {
            nextPoint = waitPoints[6];
        }
        if (point7taken == true)
        {
            nextPoint = waitPoints[7];
        }
        if (point8taken == true)
        {
            nextPoint = waitPoints[8];
        }
        if (point9taken == true)
        {
            nextPoint = waitPoints[9];
        }
    }
}
