﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class Pause : MonoBehaviour
{
    bool isPaused = false;
    public GameObject panel;

    public Button ButtonPause;
    public Sprite PauseOff;
    public Sprite PauseOn;

    // Start is called before the first frame update
    void Start()
    {
        ButtonPause = GetComponent<Button>();
        panel.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void pauseGame()
    {
        if (isPaused)
        {
            FindObjectOfType<AudioManager>().Play("Pause2");
            Time.timeScale = 1;
            isPaused = false;
            panel.gameObject.SetActive(false);
            ButtonPause.image.overrideSprite = PauseOff;
        }
        
        else
        {
            FindObjectOfType<AudioManager>().Play("Pause");
            Time.timeScale = 0;
            isPaused = true;
            panel.gameObject.SetActive(true);
            ButtonPause.image.overrideSprite = PauseOn;
        }
    }
}
