﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public Sprite pressPlayButton;
    public Sprite pressExitButton;
    public Animator anim;
    public void PlayGame()
    {
        StartCoroutine(LoadScene());
       
        FindObjectOfType<AudioManager>().Play("Boton2");
        GameObject.Find("Button").GetComponent<Image>().sprite = pressPlayButton;
       // SceneManager.LoadScene("Game");
    }

    IEnumerator LoadScene()
    {
        anim.SetTrigger("end"); 
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene("Game");
    }    

    public void QuitGame()
    {
        FindObjectOfType<AudioManager>().Play("Boton");
        GameObject.Find("Button (1)").GetComponent<Image>().sprite = pressExitButton;
        Application.Quit();
    }
}
