﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject pfb1, pfb2, pfb3, pfb4;
    int whatToSpawn;
    public float spawnRate = 2f;
    float nextSpawn = 0f;

   
    
    void Update()
    {
        if (Time.time > nextSpawn)
        {
            whatToSpawn = Random.Range(1, 5);

            switch(whatToSpawn)
            {
                case 1:
                    Instantiate(pfb1, transform.position, Quaternion.identity);
                    break;
                case 2:
                    Instantiate(pfb2, transform.position, Quaternion.identity);
                    break;
                case 3:
                    Instantiate(pfb3, transform.position, Quaternion.identity);
                    break;
                case 4:
                    Instantiate(pfb4, transform.position, Quaternion.identity);
                    break;
                    
            }
            nextSpawn = Time.time + spawnRate;
        }

    }
}
