﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anim : MonoBehaviour
{

    public Animator animComp;

    public void WalkAnim()
    {
        animComp.Play("Walk");
    }

    public void IdleAnim()
    {
        animComp.Play("Idle");
    }
}
